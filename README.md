I make a simple UI design for focusing on architecture and pure coding.
I chose VIPER architecture for making an app is more testable.
I follow the SOLID principles while developing app. 
All three requests are running simultaneously and separately as well as algorithms execution for non-freezing user interaction. 
For next I’d enhance existing tests and add tests covering utility services (Network, Parser, CounterContent with mock data). 