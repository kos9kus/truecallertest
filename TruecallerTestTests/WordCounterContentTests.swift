//
//  WordCounterContentTests.swift
//  TruecallerTestTests
//
//  Created by KONSTANTIN KUSAINOV on 07/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import XCTest
@testable import TruecallerTest

class WordCounterContentTests: XCTestCase {
    
    func testNumberOfWords() {
        let service = ContentServiceImpl.buildContentService()
        let exp = self.expectation(description: "testNumberOfWords")
        let counter = WordCounterContentImpl(contentService: service)
        counter.numberOfWords(completion: { (wordsMap) in
            XCTAssertFalse(wordsMap.isEmpty)
            print(humanPrintableWordMap(wordMap: wordsMap))
            exp.fulfill()
        }) { (error) in
            XCTFail(error.title)
        }
        self.waitForExpectations(timeout: 5)
    }
    
    func testNumberOfWordInContent() {
        let service = ContentServiceImpl.buildContentService()
        let exp = self.expectation(description: "testNumberOfWordInContent")
        let counter = WordCounterContentImpl(contentService: service)
        let word = "service"
        counter.countOfWordInContent(word: word, completion: { (count) in
            XCTAssertTrue(count == 2)
            exp.fulfill()
        }) { (error) in
            XCTFail(error.title)
        }
        self.waitForExpectations(timeout: 5)
    }

}
