//
//  CharacterCounterContentTests.swift
//  TruecallerTestTests
//
//  Created by k.kusainov on 06/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import XCTest
@testable import TruecallerTest

class CharacterCounterContentTests: XCTestCase {

    func testFindTenCharacter() {
		let service = ContentServiceImpl.buildContentService()
		let exp = self.expectation(description: "testFindTenCharacter")
		let counter = CharacterCounterContentImpl(contentService: service)
		counter.findTenCharacter(completion: { (char) in
			XCTAssertNotNil(char)
			print(char ?? "Error")
			exp.fulfill()
		}) { (error) in
			XCTFail(error.title)
		}
		self.waitForExpectations(timeout: 5)
    }
	
	func testFindAllTenableCharacter() {
		let service = ContentServiceImpl.buildContentService()
		let exp = self.expectation(description: "testFindAllTenableCharacter")
		let counter = CharacterCounterContentImpl(contentService: service)
		counter.findAllTenableCharacter(completion: { (chars) in
			XCTAssertTrue(chars.count > 0)
			print(chars)
			exp.fulfill()
		}) { (error) in
			XCTFail(error.title)
		}
		self.waitForExpectations(timeout: 5)
	}
}
