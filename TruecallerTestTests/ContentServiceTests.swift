//
//  ContentServiceTests.swift
//  TruecallerTestTests
//
//  Created by k.kusainov on 06/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import XCTest
@testable import TruecallerTest

class ContentServiceTests: XCTestCase {

    func testGrabContent() {
        let service = ContentServiceImpl.buildContentService()
		let exp = self.expectation(description: "testGrabContent")
		service.grabContent(completion: { (content) in
			print(content)
			XCTAssertTrue(content.count > 0)
			exp.fulfill()
		}) { (error) in
			XCTFail()
			exp.fulfill()
		}
		
		self.waitForExpectations(timeout: 5)
    }
}
