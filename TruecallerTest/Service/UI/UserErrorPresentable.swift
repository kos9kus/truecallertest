//
//  UserErrorPresentable.swift
//  TruecallerTest
//
//  Created by KONSTANTIN KUSAINOV on 07/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

protocol UserErrorPresentable: class {
    func presentError(error: ErrorDescriptor)
}

extension UserErrorPresentable where Self: UIViewController {
    func presentError(error: ErrorDescriptor) {
        let alertVC = UIAlertController.init(title: error.title, message: error.message, preferredStyle: .alert)
        let actionOK = UIAlertAction(title: "OK", style: .default)
        alertVC.addAction(actionOK)
        self.present(alertVC, animated: true)
    }
}
