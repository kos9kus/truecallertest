//
//  String+Ext.swift
//  TruecallerTest
//
//  Created by k.kusainov on 06/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

extension String {
	
	func index(position: Int) -> Index? {
		return index(startIndex, offsetBy: position, limitedBy: endIndex)
	}
	
	func character(position: Int) -> Character? {
		guard position >= 0, let indexPosition = index(position: position) else {
			return nil
		}
		return self[indexPosition]
	}
}
