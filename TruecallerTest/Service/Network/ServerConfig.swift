//
//  ServerConfig.swift
//  TruecallerTest
//
//  Created by k.kusainov on 06/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol ServerConfig {
	var serverDomain: String { get }
	var scheme: String { get }
}

extension NSURLComponents {
	static func request(serverConfig: ServerConfig) -> NSURLComponents {
		let components = NSURLComponents()
		components.scheme = serverConfig.scheme
		components.host = serverConfig.serverDomain
		return components
	}
}
