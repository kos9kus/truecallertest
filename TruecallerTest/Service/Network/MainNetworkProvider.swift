//
//  MainNetworkProvider.swift
//  TruecallerTest
//
//  Created by k.kusainov on 06/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

class MainNetworkProvider: NetworkProvider {
	
	typealias Request = URLRequest
	
	private let session: URLSession = {
		let sessionConfig = URLSessionConfiguration.default
        sessionConfig.requestCachePolicy = .reloadIgnoringLocalCacheData
		let session = URLSession.init(configuration: sessionConfig)
		return session
	}()
	
	func makeRequest(request: Request, completion: @escaping (Data) -> (), failure: @escaping (Network<MainNetworkProvider>.NetworkErrorResponse) -> ()) {
		let task = session.dataTask(with: request) { (data, response, error) in
			if let data = data, let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 {
				completion(data)
			} else {
				self.handleBadResponse(response: response, error: error, failure: failure)
			}
		}
		task.resume()
	}
	
	private func handleBadResponse(response: URLResponse?, error: Error?, failure: @escaping (Network<MainNetworkProvider>.NetworkErrorResponse) -> ()) {
		if let httpResponse = response as? HTTPURLResponse {
			failure(.badResponse(httpResponse))
		} else if let error = error {
			failure(.error(error))
		} else {
			failure(.error(MainNetworkProviderError()))
		}
	}
	
	struct MainNetworkProviderError: Error {
		var localizedDescription: String {
			return NSLocalizedString("Undefined network error", comment: "")
		}
	}
}
