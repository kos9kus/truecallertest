//
//  Network.swift
//  TruecallerTest
//
//  Created by KONSTANTIN KUSAINOV on 06/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol NetworkProvider {
    associatedtype Request
    func makeRequest(request: Request, completion: @escaping (Data) -> (), failure: @escaping (Network<Self>.NetworkErrorResponse) -> ())
}

final class Network<ProviderObject: NetworkProvider> {
    
    typealias Request = ProviderObject.Request
    
    private let provider: ProviderObject
    
    init(networkProvider: ProviderObject) {
        provider = networkProvider
    }
    
    func makeRequest(request: Request, completion: @escaping (Data) -> (), failure: @escaping (NetworkError) -> ()) {
        self.provider.makeRequest(request: request, completion: completion) { (errorResponse) in
            failure(Network.networkError(from: errorResponse))
        }
    }
    
    private static func networkError(from errorResponse: NetworkErrorResponse) -> NetworkError {
        switch errorResponse {
        case .badResponse(let response):
            return NetworkError(response: response)
        case .error(let error):
            return .internalError(error)
        }
    }
    
    enum NetworkErrorResponse {
        case error(Error)
        case badResponse(HTTPURLResponse)
    }
    
    internal enum NetworkError: ErrorDescriptor {
        case internalError(Error)
        case serverError
        case clientError
        case unknownError
        
        init(response: HTTPURLResponse) {
            switch response.statusCode {
            case 400...499:
                self = .clientError
            case 500...599:
                self = .serverError
            default:
                self = .unknownError
            }
        }
        
        var title: String {
            switch self {
            case .clientError:
                return NSLocalizedString("Client error", comment: "")
            case .serverError:
                return NSLocalizedString("Server error", comment: "")
            case .internalError(_):
                return NSLocalizedString("Internal error", comment: "")
            default:
                return "Error"
            }
        }
        
        var message: String {
            switch self {
            case .clientError:
                return NSLocalizedString("Incorrect request", comment: "")
            case .serverError:
                return NSLocalizedString("Server is down", comment: "")
            case .internalError(_):
                return NSLocalizedString("Network has a problem of availability", comment: "")
            default:
                return "Error"
            }
        }
        
        var verboseMessage: String {
            switch self {
            case .internalError(let error):
                return error.localizedDescription
            default:
                return ""
            }
        }
    }
    
}


