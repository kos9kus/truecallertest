//
//  MainServerConfig.swift
//  TruecallerTest
//
//  Created by k.kusainov on 06/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

struct MainServerConfig: ServerConfig {
	var serverDomain: String {
		return "www.truecaller.com"
	}
	
	var scheme: String {
		return "https"
	}
}
