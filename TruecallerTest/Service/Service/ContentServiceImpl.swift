//
//  ContentServiceImpl.swift
//  TruecallerTest
//
//  Created by k.kusainov on 06/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

class ContentServiceImpl: ContentService {
	
	typealias NetworkProviderObject = MainNetworkProvider
	
	private let parser: Parser
	private let serverConfig: ServerConfig
	private let network: Network<NetworkProviderObject>
	
	init(parser: Parser, network: Network<NetworkProviderObject>, serverConfig: ServerConfig) {
		self.parser = parser
		self.network = network
		self.serverConfig = serverConfig
	}
	
	static func buildContentService() -> ContentService {
		let mainNetworkProvider = MainNetworkProvider()
		let network = Network(networkProvider: mainNetworkProvider)
		let parser = ResultParser()
		let serverConfig = MainServerConfig()
		return ContentServiceImpl(parser: parser, network: network, serverConfig: serverConfig)
	}
	
	func grabContent(completion: @escaping (String) -> (), failure: @escaping (ErrorDescriptor) -> ()) {
		let urlComponents = NSURLComponents.request(serverConfig: self.serverConfig)
		guard let url = urlComponents.url else {
			fatalError()
		}
		let request = URLRequest.init(url: url)
		network.makeRequest(request: request, completion: { [weak self] (data) in
			guard let `self` = self else {
				return
			}
			do {
				let content = try self.parser.parse(data: data)
				completion(content)
			} catch let exp as ErrorDescriptor {
				failure(exp)
			} catch _ {
				
			}
		}, failure: failure)
	}
}
