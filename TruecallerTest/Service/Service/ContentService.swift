//
//  ContentService.swift
//  TruecallerTest
//
//  Created by k.kusainov on 06/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol ContentService {
	func grabContent(completion: @escaping (String) -> (), failure: @escaping (ErrorDescriptor) -> ())
}
