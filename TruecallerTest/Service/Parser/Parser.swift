//
//  Parser.swift
//  TruecallerTest
//
//  Created by KONSTANTIN KUSAINOV on 06/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol Parser {
    func parse(data: Data) throws -> String
}

class ResultParser: Parser {
    
    struct ParserError: ErrorDescriptor {
        var title: String {
            return "Parse error"
        }
        
        var message: String {
            return "Can't parse data"
        }
    }
    
    func parse(data: Data) throws -> String {
        guard let parsedData = String(data: data, encoding: .utf8) else {
            throw ParserError()
        }
        
        return parsedData
    }
}
