//
//  ErrorDescriptor.swift
//  TruecallerTest
//
//  Created by KONSTANTIN KUSAINOV on 06/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol ErrorDescriptor: Error {
    var title: String { get }
    var message: String { get }
    var verboseMessage: String { get }
}

extension ErrorDescriptor {
    var verboseMessage: String {
        return ""
    }
}
