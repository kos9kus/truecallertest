//
//  CharacterCounterContentImpl.swift
//  TruecallerTest
//
//  Created by k.kusainov on 06/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

class CharacterCounterContentImpl: CharacterCounterContent {
	
	private let contentService: ContentService
	
	init(contentService: ContentService) {
		self.contentService = contentService
	}
	
	func findTenCharacter(completion: @escaping (Character?) -> (), failure: @escaping (ErrorDescriptor) -> ()) {
		contentService.grabContent(completion: { (content) in
            DispatchQueue.main.async {
                completion(content.character(position: 10))
            }
		}, failure: failure)
	}
	
	func findAllTenableCharacter(completion: @escaping ([Character]) -> (), failure: @escaping (ErrorDescriptor) -> ()) {
		contentService.grabContent(completion: { (content) in
			DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
				var chars: [Character] = []
				var offset = 10
				while let char = content.character(position: offset) {
					chars.append(char)
					offset += 10
				}
                DispatchQueue.main.async {
                    completion(chars)
                }
			}
		}, failure: failure)
	}
}
