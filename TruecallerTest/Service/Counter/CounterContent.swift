//
//  CounterContent.swift
//  TruecallerTest
//
//  Created by k.kusainov on 06/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol CharacterCounterContent {
	func findTenCharacter(completion: @escaping (Character?) -> (), failure: @escaping (ErrorDescriptor) -> ())
	func findAllTenableCharacter(completion: @escaping ([Character]) -> (), failure: @escaping (ErrorDescriptor) -> ())
}

protocol WordCounterContent {
    func countOfWordInContent(word: String, completion: @escaping (UInt) -> (), failure: @escaping (ErrorDescriptor) -> ())
    func numberOfWords(completion: @escaping ([String : UInt]) -> (), failure: @escaping (ErrorDescriptor) -> ())
}


func humanPrintableCharacters(characters: [Character]) -> String {
    var humanPrintableString = ""
    for char in characters {
        humanPrintableString = humanPrintableString + String(char) + " "
    }
    return humanPrintableString
}

func humanPrintableWordMap(wordMap: [String: UInt]) -> String {
    var humanPrintableString = ""
    for (key, value) in wordMap {
        humanPrintableString = humanPrintableString + key + " == " + "\(value)" + "\n"
    }
    return humanPrintableString
}
