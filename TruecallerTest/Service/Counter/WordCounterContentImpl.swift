//
//  WordCounterContentImpl.swift
//  TruecallerTest
//
//  Created by k.kusainov on 06/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

class WordCounterContentImpl: WordCounterContent {
	
	private let contentService: ContentService
	
	init(contentService: ContentService) {
		self.contentService = contentService
	}
	
	func numberOfWords(completion: @escaping ([String : UInt]) -> (), failure: @escaping (ErrorDescriptor) -> ()) {
		contentService.grabContent(completion: { [weak self] (content) in
            guard let `self` = self else {
                return
            }
            self.numberOfWords(content: content, completion: completion)
		}, failure: failure)
	}
	
	func countOfWordInContent(word: String, completion: @escaping (UInt) -> (), failure: @escaping (ErrorDescriptor) -> ()) {
		contentService.grabContent(completion: { [weak self] (content) in
            guard let `self` = self else {
                return
            }
            self.numberOfWords(content: content, completion: { (map) in
                completion(map[word] ?? 0)
            })
		}, failure: failure)
	}
    
    private func numberOfWords(content: String, completion: @escaping ([String : UInt]) -> ()) {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            let words = content.split(whereSeparator: { (char) -> Bool in
                let setChar = CharacterSet(charactersIn: String(char))
                let setWhitespacesAndNewlines = CharacterSet.whitespacesAndNewlines.intersection(setChar)
                return setWhitespacesAndNewlines.isEmpty == false
            })
            var map: Dictionary<String, UInt> = [:]
            for index in 0...(words.count - 1) {
                let word = String(words[index])
                if let value = map[word] {
                    map[word] = (value + 1)
                } else {
                    map[word] = 1
                }
            }
            DispatchQueue.main.async {
                completion(map)
            }
        }
    }
	
}
