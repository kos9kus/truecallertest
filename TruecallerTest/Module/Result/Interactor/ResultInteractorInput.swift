//
//  ResultInteractorInput.swift
//  TruecallerTest
//
//  Created by KONSTANTIN KUSAINOV on 06/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol ResultInteractorInput: class {
    func count()
    func countOfWord(word: String)
    func countAllWords()
}
