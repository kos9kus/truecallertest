//
//  ResultInteractor.swift
//  TruecallerTest
//
//  Created by KONSTANTIN KUSAINOV on 06/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

class ResultInteractor: ResultInteractorInput {
    private unowned let presenter: ResultInteractorOutput
    private let characterCounterContent: CharacterCounterContent
    private let wordCounterContent: WordCounterContent
    private var executing: Bool = false {
        didSet {
            presenter.handleDidChangeExecuting(executing: executing)
        }
    }
    
    init(presenter: ResultInteractorOutput, characterCounterContent: CharacterCounterContent, wordCounterContent: WordCounterContent) {
        self.presenter = presenter
        self.characterCounterContent = characterCounterContent
        self.wordCounterContent = wordCounterContent
    }
    
    //MARK: ResultInteractorInput
    
    func count() {
        guard !executing else {
            return
        }
        
        executing = true
        let groupQueue = DispatchGroup.init()
        var errorDescriptor: ErrorDescriptor?
        
        groupQueue.enter()
        self.characterCounterContent.findAllTenableCharacter(completion: { [weak self] (characters) in
            guard let `self` = self else {
                return
            }
            self.presenter.handleAllTenableCharacter(characters: characters)
            groupQueue.leave()
        }) { (error) in
            errorDescriptor = error
            groupQueue.leave()
        }
        
        groupQueue.enter()
        self.characterCounterContent.findTenCharacter(completion: { [weak self] (character) in
            guard let `self` = self else {
                return
            }
            self.presenter.handleFindTenCharacter(character: character)
            groupQueue.leave()
        }) { (error) in
            errorDescriptor = error
            groupQueue.leave()
        }
        
        groupQueue.enter()
        self.wordCounterContent.numberOfWords(completion: { [weak self] (wordMap) in
            guard let `self` = self else {
                return
            }
            self.presenter.handleNumberOfWords(wordsMap: wordMap)
            groupQueue.leave()
        }) { (error) in
            errorDescriptor = error
            groupQueue.leave()
        }
        
        groupQueue.notify(qos: DispatchQoS.userInitiated, queue: DispatchQueue.main) {
            self.executing = false
            if let error = errorDescriptor {
                self.presenter.handleError(error: error)
            }
        }
    }
    
    func countAllWords() {
        guard !executing else {
            return
        }
        executing = true
        
        self.wordCounterContent.numberOfWords(completion: { [weak self] (wordMap) in
            guard let `self` = self else {
                return
            }
            self.executing = false
            self.presenter.handleNumberOfWords(wordsMap: wordMap)
        }) { [weak self] (error) in
            guard let `self` = self else {
                return
            }
            self.handleError(error: error)
        }
    }
    
    func countOfWord(word: String) {
        guard !executing else {
            return
        }
        executing = true
        
        self.wordCounterContent.countOfWordInContent(word: word, completion: { [weak self] (counter) in
            guard let `self` = self else {
                return
            }
            self.executing = false
            self.presenter.handleCountOfWordInContent(count: counter)
        }) { [weak self] (error) in
            guard let `self` = self else {
                return
            }
            self.handleError(error: error)
        }
    }
    
    private func handleError(error: ErrorDescriptor) {
        DispatchQueue.main.async {
            self.executing = false
            self.presenter.handleError(error: error)
        }
    }
}
