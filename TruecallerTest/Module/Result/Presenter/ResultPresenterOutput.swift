//
//  ResultPresenterOutput.swift
//  TruecallerTest
//
//  Created by KONSTANTIN KUSAINOV on 06/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol ResultPresenterOutput: class {
    func viewIsReady()
    func searchAction(searchText: String)
    func resetSearchAction()
    func runAction()
}
