//
//  ResultPresenter.swift
//  TruecallerTest
//
//  Created by KONSTANTIN KUSAINOV on 05/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

class ResultPresenter: ResultPresenterOutput, ResultInteractorOutput {
    
    private unowned var view: ResultViewInput
    private var router: ResultRouterInput
    var interactor: ResultInteractorInput!
    
    init(view: ResultViewInput, router: ResultRouterInput) {
        self.view = view
        self.router = router
    }
    
    //MARK: ResultPresenterOutput
    
    func viewIsReady() {
        view.resetAll()
        view.enableSpinner(enable: false)
    }
    
    func searchAction(searchText: String) {
        view.endEditing()
        interactor.countOfWord(word: searchText)
    }
    
    func resetSearchAction() {
        view.endEditing()
        view.resetSearchField()
        interactor.countAllWords()
    }
    
    func runAction() {
        self.view.resetAll()
        self.interactor.count()
    }
    
    //MARK: ResultInteractorOutput
    
    func handleError(error: ErrorDescriptor) {
        self.router.presentAlertError(alertError: error)
    }
    
    func handleAllTenableCharacter(characters: [Character]) {
        var text = "No found"
        if characters.count > 0 {
            text = humanPrintableCharacters(characters: characters)
        }
        self.view.setAllTenableCharacter(text: text)
    }
    
    func handleFindTenCharacter(character: Character?) {
        var text = "No found"
        if let char = character {
            text = String(char)
        }
        self.view.setTenCharacter(text: text)
    }
    
    func handleNumberOfWords(wordsMap: [String : UInt]) {
        let text = humanPrintableWordMap(wordMap: wordsMap)
        self.view.setNumberOfWords(text: text)
    }
    
    func handleCountOfWordInContent(count: UInt) {
        self.view.setNumberOfWords(text: "\(count)")
    }
    
    func handleDidChangeExecuting(executing: Bool) {
        self.view.enableSpinner(enable: executing)
    }
}
