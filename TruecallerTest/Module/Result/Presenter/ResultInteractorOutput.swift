//
//  ResultPresenterInput.swift
//  TruecallerTest
//
//  Created by KONSTANTIN KUSAINOV on 06/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol ResultInteractorOutput: class {
    func handleError(error: ErrorDescriptor)
    func handleAllTenableCharacter(characters: [Character])
    func handleFindTenCharacter(character: Character?)
    func handleNumberOfWords(wordsMap: [String : UInt])
    func handleCountOfWordInContent(count: UInt)
    func handleDidChangeExecuting(executing: Bool)
}
