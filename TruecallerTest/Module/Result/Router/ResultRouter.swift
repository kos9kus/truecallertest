//
//  ResultRouter.swift
//  TruecallerTest
//
//  Created by KONSTANTIN KUSAINOV on 07/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

class ResultRouter: ResultRouterInput {
    
    unowned var presentableView: UserErrorPresentable
    
    init(presentableView: UserErrorPresentable) {
        self.presentableView = presentableView
    }
    
    func presentAlertError(alertError: ErrorDescriptor) {
        self.presentableView.presentError(error: alertError)
    }
}
