//
//  ResultRouterInput.swift
//  TruecallerTest
//
//  Created by KONSTANTIN KUSAINOV on 07/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol ResultRouterInput: class {
    func presentAlertError(alertError: ErrorDescriptor)
}
