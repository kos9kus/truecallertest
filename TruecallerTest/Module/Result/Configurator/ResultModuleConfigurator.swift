//
//  ResultModuleConfigurator.swift
//  TruecallerTest
//
//  Created by KONSTANTIN KUSAINOV on 06/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

class ResultModuleConfigurator {
    class func configureModuleForViewController(viewController: UIViewController) {
        guard let view = viewController as? ResultViewController else {
            fatalError("Incorrect module")
        }
        
        let router = ResultRouter(presentableView: view)
        
        let presenter = ResultPresenter(view: view, router: router)
        view.presenter = presenter
        
        let service = ContentServiceImpl.buildContentService()
        let characterCounterContent = CharacterCounterContentImpl(contentService: service)
        let wordCounterContent = WordCounterContentImpl(contentService: service)
        let interactor = ResultInteractor(presenter: presenter, characterCounterContent: characterCounterContent, wordCounterContent: wordCounterContent)
        presenter.interactor = interactor
    }
}
