//
//  ResultModuleInitializer.swift
//  TruecallerTest
//
//  Created by KONSTANTIN KUSAINOV on 06/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

class ResultModuleInitializer: NSObject {

    @IBOutlet weak var resultViewController: ResultViewController!
    
    override func awakeFromNib() {
        ResultModuleConfigurator.configureModuleForViewController(viewController: resultViewController)
    }
}
