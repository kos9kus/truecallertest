//
//  ResultViewInput.swift
//  TruecallerTest
//
//  Created by KONSTANTIN KUSAINOV on 06/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol ResultViewInput: class {
    var searchText: String { get }
    func resetSearchField()
    func resetAll()
    func endEditing()
    func enableSpinner(enable: Bool)
    func setAllTenableCharacter(text: String)
    func setTenCharacter(text: String)
    func setNumberOfWords(text: String)
}
