//
//  ResultViewController.swift
//  TruecallerTest
//
//  Created by KONSTANTIN KUSAINOV on 05/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController, ResultViewInput, UserErrorPresentable {
    
    var presenter: ResultPresenterOutput!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var label10Char: UILabel!
    @IBOutlet weak var textViewEvery10ThChar: UITextView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var wordCounterTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewIsReady()
    }
    
    //MARK: ResultViewInput
    
    func endEditing() {
        searchTextField.resignFirstResponder()
    }
    
    var searchText: String {
        return searchTextField.text ?? ""
    }
    
    func resetAll() {
        label10Char.text = nil
        textViewEvery10ThChar.text = nil
        searchTextField.text = nil
        wordCounterTextView.text = nil
    }
    
    func resetSearchField() {
        self.searchTextField.text = nil
    }
    
    func enableSpinner(enable: Bool) {
        spinner.isHidden = !enable
    }
    
    func setAllTenableCharacter(text: String) {
        self.textViewEvery10ThChar.text = text
    }
    
    func setTenCharacter(text: String) {
        self.label10Char.text = text
    }
    
    func setNumberOfWords(text: String) {
        self.wordCounterTextView.text = text
    }
    
    //MARK: Actions
    
    @IBAction func didTapSearch(_ sender: UIButton) {
        presenter.searchAction(searchText: searchText)
    }
    
    @IBAction func didTapReset(_ sender: UIButton) {
        presenter.resetSearchAction()
    }
    
    @IBAction func didTapRun(_ sender: UIButton) {
        presenter.runAction()
    }
}
